export default class TodoItemData {
    constructor(id, title, isDone = false) {
        this.id = id;
        this.title = title;
        this.isDone = isDone;
    }

    check() {
        this.isDone = !this.isDone;
    }
}