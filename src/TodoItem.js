import * as React from "react";

export default class TodoItem extends React.Component {
    render() {
        return (
            <label className="c-todo-item">
                <input
                    name="todoItemCheckbox"
                    type="checkbox"
                    value={this.props.item.isDone}
                    onChange={this.props.onChecked} />

                {this.props.item.title}
            </label>
        );
    }
}