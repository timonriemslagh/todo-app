import * as React from "react";
import TodoItemData from "./TodoItemData";
import TodoItem from "./TodoItem";

export default class TodoBoard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentId: 4,
            todoItems: [
                new TodoItemData(1, 'Was doen'),
                new TodoItemData(2, 'Was uithalen'),
                new TodoItemData(3, 'Was ophangen'),
            ],
            newTodoItem: '',
        }
    }

    handleChecked(todoId) {
        const newItems = this.state.todoItems.slice();

        for (let item of newItems) {
            if (item.id === todoId) {
                item.check();
            }
        }

        this.setState({
            todoItems: newItems,
        });
    }

    // Upgrade this function to return a guid
    getNewId() {
        return this.state.currentId + 1;
    }

    addTodo(e) {
        e.preventDefault();

        if (this.state.newTodoItem === '') {
            return;
        }

        const newTodo = new TodoItemData(this.state.currentId, this.state.newTodoItem);
        const allTodos = this.state.todoItems.slice();

        allTodos.push(newTodo);

        // Do you have to remember whats in the state when you update?
        this.setState({
            currentId: this.getNewId(),
            todoItems: allTodos,
            newTodoItem: '',
        });
    }

    handleChange(e) {
        this.setState({
            newTodoItem: e.target.value
        });
    }

    render() {
        return (
            <div className="c-todo-board">
                <div>
                <h2>Items done</h2>
                <div>
                {
                    this.state.todoItems
                        .filter(val => val.isDone)
                        .map((todoItem) => {
                            return (<div className="c-item-done" key={todoItem.id}>
                                <p>{todoItem.title}</p>
                                <a href="#" className="c-link" onClick={() => this.handleChecked(todoItem.id)}>undo</a>
                            </div>);
                    })
                }
                </div>

                <h2>Items todo</h2>
                {
                    this.state.todoItems
                        .filter(val => !val.isDone)
                        .map((todoItem) => {
                        return <TodoItem
                            key={todoItem.title}
                            item={todoItem}
                            onChecked={() => this.handleChecked(todoItem.id)}
                        />
                    })
                }

                <form className="c-form" onSubmit={e => this.addTodo(e)}>
                    <input type="text" className="c-input-text" placeholder="Got something to do?" value={this.state.newTodoItem} onChange={e => this.handleChange(e)} />
                    <input type="submit" className="c-button" value="Add" />
                </form>
                </div>
            </div>
        );
    }
}