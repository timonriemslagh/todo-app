import React from 'react';
import TodoBoard from "./TodoBoard";

function App() {
  return (
    <div className="App">
      <TodoBoard />
    </div>
  );
}

export default App;
